# laravel-elasticsearch
Implementação do Tutorial https://madewithlove.be/how-to-integrate-elasticsearch-in-your-laravel-app-2019-edition/

## Baixando
```bash
git clone git@gitlab.com:moreiraandre/laravel-elasticsearch.git
cd laravel-elasticsearch
```

## Configurando Laravel
Presume-se que:
* **Composer** esteja instalado globalmente;
* **Elasticsearch** esteja instalado na máquina.
```bash
cp .env.example .env
composer install
php artisan key:generate
php artisan migrate --seed
```

Caso haja dúvidas, siga o tutorial indicado no início.
