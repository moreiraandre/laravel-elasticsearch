<?php

/**
 * Model que será utilizado para inserir registros no banco relacional, buscar os registros do banco relacioal para
 * inserir no Easticsearch através de comando do Artisan e terá seus eventos observados para sincronizar os registros do
 * banco relacional com o Elasticsearch.
 */

namespace App;

use App\Search\Searchable;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use Searchable;

    protected $casts = [
        // OS DADOS SERÃO GRAVADOS NO CAMPO tags COMO ARRAY DO PHP, E QUANDO FOREM RECUPERADOS SERÃO CONVERTIDOS P/ JSON
        'tags' => 'json',
    ];
}
