<?php

/**
 * Popula a tabela articles com dados fake.
 */

use Illuminate\Database\Seeder;

class ArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('articles')->truncate(); // REMOVE OS REGISTROS DA TABELA articles
        factory(App\Article::class)->times(50)->create(); // INSERE 50 REGISTROS FAKES NA TABELA articles
    }
}
