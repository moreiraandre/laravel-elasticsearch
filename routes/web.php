<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Elasticsearch\ClientBuilder;
// RETORNANDO PARA O USUÁRIO TODOS OS REGISTROS DO BANCO RELACIONAL
Route::get('/', function () {
    return view('articles.index', [
        'articles' => App\Article::all(),
    ]);
});

use App\Articles\ArticlesRepository;
// RETORNANDO PARA O USUÁRIO OS REGISTROS FILTRADOS NO ELASTICSEARCH
Route::get('/search', function (ArticlesRepository $repository) {
    $articles = $repository->search((string)request('q')); // FILTRANDO OS REGISTROS COM A VARIÁVEL q VINDA NA URL

    return view('articles.index', [
        'articles' => $articles,
    ]);
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
